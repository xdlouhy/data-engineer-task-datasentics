import requests
from typing import List
import zipfile

url = "http://www2.informatik.uni-freiburg.de/~cziegler/BX/BX-CSV-Dump.zip"
request = requests.get(url, allow_redirects=True)
open("data.zip", "wb").write(request.content)

with zipfile.ZipFile("./data.zip", 'r') as file:
    file.extractall("./Downloads")


def get_lines(path: str) -> List[str]:
    with open(path, 'r', encoding='iso-8859-15') as data:
        return data.readlines()


def refine(path: str, lines: List[str]) -> None:
    header = list(map(str.strip, lines[0].split(';')))
    fields_count = len(header)
    with open(path, 'w', encoding='iso-8859-15') as data:
        rating_index = header.index("\"Book-Rating\"") if "\"Book-Rating\"" in header else -1
        for line in lines:
            fields = line.split(';')
            if len(fields) == fields_count and (rating_index < 0 or fields[rating_index].strip() != "\"0\""):
                data.write(line)


for path in "./Downloads/BX-Books.csv", "./Downloads/BX-Book-Ratings.csv":
    lines = get_lines(path)
    refine(path, lines)
