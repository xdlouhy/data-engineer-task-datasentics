from typing import List, Dict

Row = List[str]


class Database:
    def __init__(self, fields: List[str], primary_key):
        self.fields: List[str] = fields
        self.rows = {}
        self.primary_key = primary_key


def read(database: Database, input) -> None:
    line: str

    while line := input.readline():
        row = {}
        line_split = list(map((lambda x: x[1:len(x) - 1]), line.strip().split(';')))

        if len(line_split) == len(database.fields):
            for i, field in enumerate(line_split):
                row.update({database.fields[i]: field})
            key = []
            for field in database.primary_key:
                key.append(row.get(field))
            key = tuple(list(map(lambda x: x.lower(), key)))
            try:
                database.rows[key]
            except KeyError:
                database.rows[key] = []
            finally:
                database.rows[key].append(row)


def create_database(primary_key, path: str) -> Database:
    with open(path, mode='r', encoding='iso-8859-15') as input:
        fields = list(map((lambda x: x[1:len(x) - 1]), input.readline().strip().split(';')))
        database = Database(fields, primary_key)
        read(database, input)
    return database


def get_row(database: Database, key: List[str]):
    return database.rows.get(tuple(key))


print("Loading input.", end="")
ratings = create_database(["ISBN"], "./Downloads/BX-Book-Ratings.csv")
print(".", end="")
books = create_database(["Book-Title"], "./Downloads/BX-Books.csv")
print(".")
authors = create_database(["Book-Author"], "./Downloads/BX-Books.csv")

while True:
    name = input("\nPlease input a book name to show recommendations; type :q! to exit\n").lower()
    if name == ":q!":
        exit(0)
    publications = get_row(books, [name])

    if not publications:
        print("Sorry, the entered book is not in our database!\n")

    else:
        print("Listing books by: " + publications[0]["Book-Author"])
        author = (publications[0]["Book-Author"]).lower()
        same_author = get_row(authors, [author])

        print("{: <20} {: <120} {: <60} {: <20}".format(*["ISBN", "Name", "Publisher", "Average rating"]))

        for book in same_author:
            book_ratings = get_row(ratings, [book["ISBN"].lower()])
            rating = 0
            if book_ratings:
                rating = sum(list(map(lambda x: float(x["Book-Rating"]), book_ratings))) / len(book_ratings)
            print("{: <20} {: <120} {: <60} {:.4f}".format(
                *[book["ISBN"], book["Book-Title"], book["Publisher"], rating]))
