# import
import datetime
import os

import pandas as pd
import numpy as np
import socket


def load_database():
    # load ratings
    ratings = pd.read_csv('Downloads/BX-Book-Ratings.csv', encoding='cp1251', sep=';')

    # load books
    books = pd.read_csv('Downloads/BX-Books.csv', encoding='cp1251', sep=';')

    dataset = pd.merge(ratings, books, on=['ISBN'])
    dataset_lowercase = dataset.apply(lambda x: x.str.lower() if (x.dtype == 'object') else x)
    return books, dataset_lowercase


def recommend(name: str, author: str, books, dataset_lowercase):
    readers = dataset_lowercase['User-ID'][
        (dataset_lowercase['Book-Title'] == name) & (
            dataset_lowercase['Book-Author'].str.contains(author))]
    readers = readers.tolist()
    readers = np.unique(readers)

    # final dataset
    books_of_readers = dataset_lowercase[(dataset_lowercase['User-ID'].isin(readers))]

    # Number of ratings per other books in dataset
    number_of_rating_per_book = books_of_readers.groupby(['Book-Title']).agg('count').reset_index()

    # select only books which have actually higher number of ratings than threshold
    books_to_compare = number_of_rating_per_book['Book-Title'][number_of_rating_per_book['User-ID'] >= 8]
    books_to_compare = books_to_compare.tolist()

    ratings_data_raw = books_of_readers[['User-ID', 'Book-Rating', 'Book-Title']][
        books_of_readers['Book-Title'].isin(books_to_compare)]

    # group by User and Book and compute mean
    ratings_data_raw_nodup = ratings_data_raw.groupby(['User-ID', 'Book-Title'])['Book-Rating'].mean()

    # reset index to see User-ID in every row
    ratings_data_raw_nodup = ratings_data_raw_nodup.to_frame().reset_index()

    dataset_for_corr = ratings_data_raw_nodup.pivot(index='User-ID', columns='Book-Title', values='Book-Rating', )

    book = name
    result_list = []

    try:
        dataset_of_other_books = dataset_for_corr.copy(deep=False)
        dataset_of_other_books.drop([book], axis=1, inplace=True)

        # empty lists
        book_titles = []
        authors = []
        correlations = []
        avgrating = []

        # corr computation
        for book_title in list(dataset_of_other_books.columns.values):
            book_titles.append(book_title)
            book_author = books['Book-Author'][books['Book-Title'].str.lower() == book_title]
            book_author = np.unique(book_author)
            authors.append(book_author[0])
            correlations.append(dataset_for_corr[book].corr(dataset_of_other_books[book_title]))
            tab = (ratings_data_raw[ratings_data_raw['Book-Title'] == book_title].groupby(
                ratings_data_raw['Book-Title']).mean())
            avgrating.append(tab['Book-Rating'].min())

        # final dataframe of all correlation of each book
        corr_this = pd.DataFrame(list(zip(book_titles, authors, correlations, avgrating)),
                                 columns=['Book', 'Author', 'Correlation', 'Average rating'])
        corr_this.head()

        # top 10 books with highest corr
        result_list.append(corr_this.sort_values('Correlation', ascending=False).head(10))

        if len(result_list) > 0:
            return result_list[0].to_html(index=False)
        return None

    except KeyError:
        return None


server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.bind(('localhost', 7852))
server.listen(5)
books, ratings_data = load_database()
ack_packet = "HTTP/1.1 200 OK\r\nAccess-Control-Allow-Origin: *\r\nConnection: Keep-Alive\r\nContent-Type: text/html\r\n\r\n"
error_packet =  "HTTP/1.1 400 Bad Request\r\nAccess-Control-Allow-Origin: *\r\nConnection: Keep-Alive\r\nContent-Type: text/html; charset=utf-8"
not_found_packet = "HTTP/1.1 404 Not Found\r\nAccess-Control-Allow-Origin: *\r\nConnection: Keep-Alive\r\nContent-Type: text/html; charset=utf-8"
print("Server started at localhost:3000 !")

while True:
    print("Waiting for commands! : " + str(datetime.datetime.now().strftime("%H:%M:%S")))
    client_socket, address = server.accept()
    received = client_socket.recv(1024).decode()
    received_split = received.split("\n")[0].split(" ")[1][1:].split("&;")
    print("Command received: " + received_split[0])
    if len(received_split) == 1 and received_split[0] == "update":
        print("Update started!")
        os.system("python ./data_refiner.py")
        books, ratings_data = load_database()
        print("Update complete!")
        client_socket.sendall(ack_packet.encode())

    elif len(received_split) == 3 and received_split[0] == "rec":
        html = recommend(received_split[1].lower(), received_split[2].lower(), books, ratings_data)
        if html:
            print("Recommendations for " + received_split[1] + " by " + received_split[2])
            client_socket.sendall((ack_packet + html).encode())
        else:
            client_socket.sendall(not_found_packet.encode())
            print("Book not found:" + received_split[1] + " by " + received_split[2])
    else:
        print(received)
        client_socket.sendall(error_packet.encode())
        print("Invalid command entered")
