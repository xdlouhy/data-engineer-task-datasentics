import './App.css';
import Table from './Table';
import React, {useState} from 'react';
import axios from 'axios'

const App = () => {
  const [table, setTable] = useState("")
  const [author, setAuthor] = useState("")
  const [book, setBook] = useState("")
  const [loading, setLoading] = useState(false)
  const [hidden,hideTable] = useState(true)


  const Fetch = async (type) => {
      hideTable(false)
      console.log(type)
       try {
        setLoading(true)
        let response = await axios.get(`http://localhost:7852/${type}`)
        console.log("res:", response)
        setLoading(false)
        setTable(response)

      } catch(error) {
        console.log("err:", error)
        setTable("")
        setLoading(false)
      }
  }


  return (
    <div className="App">
      <div className="Heading"><h1>Book recommendation engine</h1></div>
      <div className="Navigation">
        <div className="Navigation">
          <input type="text" placeholder="Title" onChange={(ex) => {setBook(ex.target.value)}} value={book}></input>
          <input type="text" placeholder="Author" onChange={(e) => {setAuthor(e.target.value)}} value={author}></input>
          <button onClick={() => {Fetch(`rec&;${book}&;${author}`)}}>Find</button>
        </div>
        <button onClick={() => {Fetch("update")}}>Update</button>
      </div>
      {hidden ? <></> : (loading ? <div className="Table"><h1>Loading...</h1></div> : <Table content={table}/>)}
    </div>
  );
  
}

export default App;
