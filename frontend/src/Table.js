import React from 'react'
import HTMLParser from 'html-react-parser'

const Table = (props) => {
    console.log("response in Table component", props.content)
    
    if(props.content.data) {
        console.log(props.content.data)
        return(
            <div className="Table">
            {HTMLParser (props.content.data)}
            </div>
        )
    }
    else {
        return (<div className="Table"><h1>Not found!</h1></div>)
    }
}

export default Table
